<?php

class AvaliacaoDTO {

    private $idavaliacao;
    private $pintura;
    private $idusuario;

    function getIdavaliacao() {
        return $this->idavaliacao;
    }

    function getPintura() {
        return $this->pintura;
    }

    function getIdusuario() {
        return $this->idusuario;
    }

    function setIdavaliacao($idavaliacao) {
        $this->idavaliacao = $idavaliacao;
    }

    function setPintura($pintura) {
        $this->pintura = $pintura;
    }

    function setIdusuario($idusuario) {
        $this->idusuario = $idusuario;
    }

}
