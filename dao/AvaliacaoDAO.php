<?php
#Funcao para importar classes quando vem do controller e é feito o include na pagina index.php
if (!strpos(getcwd(),"controller"))
    $dir_base = "";
else
    $dir_base = "../";

require_once 'Conexao.php';
require_once $dir_base.'dto/AvaliacaoDTO.php';

class AvaliacaoDAO {

    public $pdo = null;

    public function __construct() {
        $this->pdo = Conexao::getInstance();
    }

    public function getAllAvaliacao() {
        try {
            $sql = "SELECT * FROM usuario";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();
            $avaliacoes = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $avaliacoes;
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }

    public function salvarAvaliacao(AvaliacaoDTO $avaliacao) {
        try {
            $sql = "INSERT INTO avalicao (idusuario,pintura) 
                    VALUES (?,?)";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(1, $avaliacao->getIdusuario());
            $stmt->bindValue(2, $avaliacao->getPintura());
          
            return $stmt->execute();
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }

    public function excluirAvaliacao($idavaliacao) {
        try {
            $sql = "DELETE FROM avaliacao 
                   WHERE idavaliacao = ?";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(1, $idavaliacao);
            $stmt->execute();
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }

    public function getAvaliacaoById($idavaliacao) {
        try {
            $sql = "SELECT * FROM usuario WHERE idusuario = ?";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(1, $idavaliacao);
            $stmt->execute();
            $avaliacao = $stmt->fetch(PDO::FETCH_ASSOC);
            return $avaliacao;
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }

    public function updateAvaliacao(AvaliacaoDTO $avaliacao) {
        try {
            $sql = "UPDATE avaliacao SET avaliacao=?,
                                       senha=?,
                                       perfil_idperfil=?
                    WHERE idavaliacao= ?";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(1, $avaliacao->getUsuario());
            $stmt->bindValue(2, $avaliacao->getSenha());
            $stmt->bindValue(3, $avaliacao->getPerfil_idperfil());
            $stmt->bindValue(4, $avaliacao->getIdusuario());
            $stmt->execute();
            
            
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }

}

?>
