<?php
#Funcao para importar classes quando vem do controller e é feito o include na pagina index.php
if (!strpos(getcwd(),"controller"))
    $dir_base = "";
else
    $dir_base = "../";

require_once 'Conexao.php';
require_once $dir_base.'dto/UsuarioDTO.php';

class UsuarioDAO {

    public $pdo = null;

    public function __construct() {
        $this->pdo = Conexao::getInstance();
    }

    public function getAllUsuario() {
        try {
            $sql = "SELECT u.idusuario, u.usuario, p.perfil   
                    FROM usuario u, perfil p
                    WHERE p.idperfil = u.perfil_idperfil";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();
            $usuarios = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $usuarios;
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }

    public function salvarUsuario(UsuarioDTO $usuario) {
        try {
            $sql = "INSERT INTO usuario (usuario,senha,perfil_idperfil) 
                    VALUES (?,?,?)";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(1, $usuario->getUsuario());
            $stmt->bindValue(2, $usuario->getSenha());
            $stmt->bindValue(3, $usuario->getPerfil_idperfil());
            return $stmt->execute();
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }

    public function excluirUsuario($idusuario) {
        try {
            $sql = "DELETE FROM usuario 
                   WHERE idusuario = ?";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(1, $idusuario);
            $stmt->execute();
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }

    public function getUsuarioById($idusuario) {
        try {
            $sql = "SELECT * FROM usuario WHERE idusuario = ?";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(1, $idusuario);
            $stmt->execute();
            $usuario = $stmt->fetch(PDO::FETCH_ASSOC);
            return $usuario;
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }

    public function updateUsuario(UsuarioDTO $usuario) {
        try {
            $sql = "UPDATE usuario SET usuario=?,
                                       senha=?,
                                       perfil_idperfil=?
                    WHERE idusuario= ?";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(1, $usuario->getUsuario());
            $stmt->bindValue(2, $usuario->getSenha());
            $stmt->bindValue(3, $usuario->getPerfil_idperfil());
            $stmt->bindValue(4, $usuario->getIdusuario());
            $stmt->execute();
            
            
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }

}

?>
