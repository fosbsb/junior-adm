<!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <p> </p>
          <p> </p>
        </div>
        <div class="pull-left info">
          <p><?php echo $perfil?></p>
          
        </div>
      </div>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU PRINCIPAL</li>
        <?php if ($perfil == "Administrador") { ?>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Usuário</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="index.php?acao=usuario/novo-usuario&editar=false"><i class="fa fa-circle-o"></i> Novo</a></li>
            <li><a href="index.php?acao=usuario/listar-usuario"><i class="fa fa-circle-o"></i> Listar</a></li>
          </ul>
        </li>
        <?php } ?>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i>
            <span>Avaliação</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="index.php?acao=avaliacao/nova-avaliacao&editar=false"><i class="fa fa-circle-o"></i> Nova</a></li>
            <li><a href="index.php?acao=avaliacao/listar-avaliacao&editar=false"><i class="fa fa-circle-o"></i> Listar</a></li>
          </ul>
        </li>
      </ul>