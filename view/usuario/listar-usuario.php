<?php

  require_once "dao/UsuarioDAO.php";

  $usuarioDAO = new UsuarioDAO();
  $usuarios = $usuarioDAO->getAllUsuario();
?>

<div class="col-xs-12">

<?php 

  if (isset($_REQUEST["msg"])) { 
    switch ($_REQUEST["msg"]){
      case '1':
        $msg  = "Registro cadastrado com sucesso.";
        $tipo = "success";
      break;
      case '2':
        $msg  = "Registro alterado com sucesso.";
        $tipo = "success";
      break;
      case '3':
        $msg  = "Registro excluído com sucesso.";
        $tipo = "danger";
      break;
      case '4':
        $msg = "Erro desconhecido, operação não realizada.";
        $tipo = "warning";
      break;
    }
  ?>
    <div class="alert alert-<?php echo $tipo;?> alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h4><i class="icon fa fa-info"></i> Alerta!</h4>
      <?php echo $msg;?>
    </div>
<?php } ?>

  <div class="box">
  <div class="box-header">
    <h3 class="box-title">Cadastro de Usuários</h3>
  </div>
  <!-- /.box-header -->
    <div class="box-body">
      <table id="example2" class="table table-bordered table-hover">
        <thead>
        <tr>
          <th width="10%">ID</th>
          <th>Usuário</th>
          <th>Perfil</th>
          <th width="10%">Ações</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($usuarios as $usuario){ ?> 

        <tr>
          <td><?php echo $usuario['idusuario']?></td>
          <td><?php echo $usuario['usuario']?></td>
          <td><?php echo $usuario['perfil']?></td>
          <td>
            <div class="btn-group">
              <button type="button" class="btn btn-primary btn-sm" title="Editar" onclick='location.href="index.php?acao=usuario/novo-usuario&id=<?php echo $usuario['idusuario']?>"'><i class="fa fa-pencil"></i></button>
              <button type="button" class="btn btn-danger btn-sm" title="Excluir" onclick='location.href="controller/usuario-controller.php?acao=excluir&id=<?php echo $usuario['idusuario']?>"'><i class="fa fa-trash-o"></i></button>
            </div>
          </td>
        </tr>
        <?php } ?>
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
</div>