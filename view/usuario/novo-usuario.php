        <?php
          if (isset($_REQUEST['id'])){
            $id   = $_REQUEST['id'];
            $acao = "editar";

            require_once "dao/UsuarioDAO.php";

            $usuarioDao = new UsuarioDAO();
            $user = $usuarioDao->getUsuarioById($id);
            
            $editUsuario = $user['usuario'];
            $editPerfil  = $user['perfil_idperfil'];
          }else{
            $acao = "novo";
          }

        ?>
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Cadastro Usuários</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="controller/usuario-controller.php?acao=<?php echo $acao?>" method="post">
              <input type="hidden" name="id" value="<?php echo $id?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="usuarioInput">Usuário</label>
                  <input type="text" name="usuario" required="required" value="<?php echo $editUsuario?>" class="form-control" id="usuarioInput" size="50" placeholder="Digite Usuário">
                </div>

                <div class="form-group">
                  <label for="senhaInput">Password</label>
                  <input type="password" name="senha" required="required" class="form-control" id="senhaInput" placeholder="Password">
                </div>

                <div class="form-group">
                  <div class="radio">
                    <label>
                      <input type="radio" required="required" name="perfil" id="perfil" value="1" <?php if ($editPerfil=="1") echo "checked"?>>
                      Administrador
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" required="required" name="perfil" id="perfil" value="2" <?php if ($editPerfil=="2") echo "checked"?>>
                      Avaliador
                    </label>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Cadastrar</button>
              </div>
            </form>
          </div>
          <!-- /.box -->      
        </div>  