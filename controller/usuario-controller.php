<?php

#Funcao para importar classes quando vem do controller e é feito o include na pagina index.php
if (!strpos(getcwd(),"controller"))
    $dir_base = "";
else
    $dir_base = "../";

include_once $dir_base.'dao/UsuarioDAO.php';
require_once $dir_base.'dto/UsuarioDTO.php';

// recuperei os dados do formulario
$id 	 = $_REQUEST["id"];
$acao    = $_REQUEST["acao"];

$usuario = $_POST["usuario"];
$senha   = md5($_POST["senha"]);
$perfil  = $_POST["perfil"];

$usuarioDAO = new UsuarioDAO();

if ($acao == "novo"){
	$usuarioDTO = new UsuarioDTO();
	$usuarioDTO->setUsuario($usuario);
	$usuarioDTO->setSenha($senha);
	$usuarioDTO->setPerfil_idperfil($perfil);

	$sucesso = $usuarioDAO->salvarUsuario($usuarioDTO);
	$pagina  = "acao=usuario/listar-usuario&msg=1";
	header('Location: ../index.php?'.$pagina);
	exit();
}elseif ($acao == "editar"){
	$usuarioDTO = new UsuarioDTO();
	$usuarioDTO->setIdusuario($id);
	$usuarioDTO->setUsuario($usuario);
	$usuarioDTO->setSenha($senha);
	$usuarioDTO->setPerfil_idperfil($perfil);

	$sucesso = $usuarioDAO->updateUsuario($usuarioDTO);
	$pagina  = "acao=usuario/listar-usuario&msg=2";
	header('Location: ../index.php?'.$pagina);
	exit();
}elseif ($acao == "excluir"){
	$sucesso = $usuarioDAO->excluirUsuario($id);
	$pagina  = "acao=usuario/listar-usuario&msg=3";
	header('Location: ../index.php?'.$pagina);
	exit();
}else{
	$pagina  = "acao=usuario/listar-usuario&msg=4";
	header('Location: ../index.php?'.$pagina);
	exit();
}


?>