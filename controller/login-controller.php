<?php

session_start();
require_once '../dao/LoginDAO.php';

$usuario = $_POST["usuario"];
$senha   = md5($_POST["senha"]);

$loginDAO = new LoginDAO();

if (empty($usuario) || empty($senha)){
	header('Location: ../view/login.php?erro=1');
	session_destroy();
	exit();
}else{
	$loginUsuario  = $loginDAO->login($usuario, $senha);
}

if (!empty($loginUsuario)) {
    $_SESSION["usuario"]  = $loginUsuario["usuario"];
    $_SESSION["perfil"]   = $loginUsuario["perfil"];
    $_SESSION["idperfil"] = $loginUsuario["idperfil"];

    header('Location: ../index.php');
	exit();
} else {
	session_destroy();
    header('Location: ../view/login.php?erro=1');
	exit;
}


?>